package org.com.exception;

public class UserExceptionResponse {

	private String errorMessage;
	private String requestedURI;

	public UserExceptionResponse() {

	}

	public UserExceptionResponse(String errorMessage, String requestedURI) {
		super();
		this.errorMessage = errorMessage;
		this.requestedURI = requestedURI;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getRequestedURI() {
		return requestedURI;
	}

	public void setRequestedURI(String requestedURI) {
		this.requestedURI = requestedURI;
	}

	@Override
	public String toString() {
		return errorMessage;
	}

}
