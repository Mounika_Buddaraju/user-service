package org.com.exception;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@SuppressWarnings("serial")
public class UserException extends Exception {
	private static final Logger log = LogManager.getLogger(UserException.class);

	public UserException(String message) {
		super(message);
		log.info(message);
	}
}
