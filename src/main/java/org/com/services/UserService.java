package org.com.services;

import java.util.List;
import org.com.entities.User;
import org.com.exception.UserException;
import org.com.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {
	@Autowired
	UserRepository userRepository;

	public List<User> getAllUsers() {
		return userRepository.findAll();
	}

	public User getUserById(long id) throws UserException {
		return userRepository.findById(id).orElseThrow(() -> new UserException("USER NOT FOUND WITH ID :" + id));
	}

	public User createUser(User user) {
		return userRepository.save(user);
	}

	public void deleteUser(long id) throws UserException {
		User user = userRepository.findById(id).orElseThrow(() -> new UserException("USER NOT FOUND WITH ID :" + id));
		userRepository.delete(user);
	}

	public User updateUser(long id, User user) throws UserException {
		if (id != user.getId()) {
			throw new UserException("USER NOT FOUND WITH ID :" + id);
		}
		userRepository.save(user);
		return user;
	}

}
