package org.com.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@Entity
@ApiModel(description = "All details about User")
public class User {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@ApiModelProperty(notes = "The database auto generated User ID", required = true)
	long id;

	@Size(max = 100, message = "User name must be less than or equal to 100 characters")
	@ApiModelProperty(notes = "The Name of User", required = true)
	String name;

	@NotBlank(message = "mail Id must not be blank")
	@Size(max = 254, message = "Mail Id must be less than or equal to 254 characters")
	@ApiModelProperty(notes = "Mail Id of the User", required = true)
	String mailId;

	public User() {

	}

	public User(long id, String name, String mailId) {
		super();
		this.id = id;
		this.name = name;
		this.mailId = mailId;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMailId() {
		return mailId;
	}

	public void setMailId(String mailId) {
		this.mailId = mailId;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", name=" + name + ", mailId=" + mailId + "]";
	}

}