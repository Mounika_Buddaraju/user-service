package org.com.restcontrollers;

import javax.servlet.http.HttpServletRequest;
import org.com.entities.User;
import org.com.exception.UserException;
import org.com.exception.UserExceptionResponse;
import org.com.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@Api(description = "This is a simple User Service System.we can use this Api to add, delete, update and read the specified User.")
public class UserRestController {

	@Autowired
	UserService userSevice;

	@GetMapping("/users")
	@ApiOperation(value = "Display all Users", response = Iterable.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Users fetched successfully") })
	public ResponseEntity<?> getAllUsers() {
		return new ResponseEntity<>(userSevice.getAllUsers(), HttpStatus.OK);
	}

	@GetMapping("/users/{id}")
	@ApiOperation(value = "Find User by Id", response = User.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "User fetched successfully"),
			@ApiResponse(code = 404, message = "User Not found",response = UserExceptionResponse.class) })
	public ResponseEntity<?> getUserById(@PathVariable long id, final HttpServletRequest request) {
		try {
			return new ResponseEntity<>(userSevice.getUserById(id), HttpStatus.OK);
		} catch (UserException exception) {
			return new ResponseEntity<>(new UserExceptionResponse(exception.getMessage(), request.getRequestURI()),
					HttpStatus.NOT_FOUND);
		}
	}

	@PostMapping("/users")
	@ApiOperation(value = "Adding a new User", response = User.class)
	@ApiResponses(value = { @ApiResponse(code = 201, message = "New User Created") })
	public ResponseEntity<?> createUser(@RequestBody User user) {
		return new ResponseEntity<>(userSevice.createUser(user), HttpStatus.CREATED);
	}

	@DeleteMapping("/users/{id}")
	@ApiOperation(value = "Deleting a User by Id", response = User.class)
	@ApiResponses(value = { @ApiResponse(code = 204, message = "User deleted successfully"),
			@ApiResponse(code = 404, message = "User Not found", response = UserExceptionResponse.class) })
	public ResponseEntity<?> deleteUser(@PathVariable long id, final HttpServletRequest request) {
		try {
			userSevice.deleteUser(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (UserException exception) {
			return new ResponseEntity<>(new UserExceptionResponse(exception.getMessage(), request.getRequestURI()),
					HttpStatus.NOT_FOUND);
		}
	}

	@PutMapping("/users/{id}")
	@ApiOperation(value = "Updating a User by Id", response = User.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "User Updated successfully"),
			@ApiResponse(code = 404, message = "User Not found", response = UserExceptionResponse.class) })
	public ResponseEntity<?> updateUser(@PathVariable long id, @RequestBody User user,
			final HttpServletRequest request) {
		try {
			return new ResponseEntity<>(userSevice.updateUser(id, user), HttpStatus.OK);
		} catch (UserException exception) {
			return new ResponseEntity<>(new UserExceptionResponse(exception.getMessage(), request.getRequestURI()),
					HttpStatus.NOT_FOUND);
		}
	}
}
