package org.com.controller;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import java.util.ArrayList;
import java.util.List;
import org.com.entities.User;
import org.com.restcontrollers.UserRestController;
import org.com.services.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import com.fasterxml.jackson.databind.ObjectMapper;

@WebMvcTest
class ControllerTest {
	@Autowired
	private MockMvc mockMvc;
	@Autowired
	UserRestController userController;
	@MockBean
	UserService userService;

	List<User> userList;
	User user;

	@BeforeEach
	public void setup() {
		userList = new ArrayList<>();
		userList.add(new User(1, "mouni", "mouni@gmail.com"));
		userList.add(new User(2, "bhanu", "bhanu@gmail.com"));
	}

	@Test
	public void testGetAllUsers() throws Exception {
		when(userService.getAllUsers()).thenReturn(userList);
		this.mockMvc.perform(get("/users")).andExpect(status().isOk());
	}

	@Test
	public void testGetUserById() throws Exception {
		when(userService.getUserById(1)).thenReturn(new User(1, "mouni", "mouni@gmail.com"));
		this.mockMvc.perform(get("/users/1")).andExpect(status().isOk());
	}

	@Test
	public void testDeleteUser() throws Exception {
		doNothing().when(userService).deleteUser(1);
		this.mockMvc.perform(delete("/users/1")).andExpect(status().isNoContent());
	}

	@Test
	public void testCreateUser() throws Exception {
		when(userService.createUser(ArgumentMatchers.any())).thenReturn(new User(2, "mouni", "mouni@gmail.com"));
		mockMvc.perform(post("/users").contentType("application/json")
				.content(asJsonString(new User(2, "mouni", "mouni@gmail.com")))).andExpect(status().isCreated());
	}

	@Test
	public void testUpdateUser() throws Exception {
		when(userService.updateUser(1, user)).thenReturn(new User(1, "mouni", "mouni@gmail.com"));
		this.mockMvc.perform(put("/users/1").contentType("application/json")
				.content(asJsonString(new User(1, "mouni", "mouni@gmail.com")))).andExpect(status().isOk());
	}

	public static String asJsonString(final Object obj) {
		try {
			return new ObjectMapper().writeValueAsString(obj);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}