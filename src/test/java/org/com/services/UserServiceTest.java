package org.com.services;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.when;
import static org.assertj.core.api.Assertions.assertThatCode;
import static org.junit.jupiter.api.Assertions.assertTrue;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.com.entities.User;
import org.com.exception.UserException;
import org.com.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

public class UserServiceTest {

	@Mock
	UserRepository userRepository;

	@InjectMocks
	UserService userService;

	@BeforeEach
	public void init() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void getAllUsersTest() {
		List<User> userList = new ArrayList<>();
		userList.add(new User(1, "mouni", "mouni@gmail.com"));
		when(userRepository.findAll()).thenReturn(userList);
		List<User> actualList = userService.getAllUsers();
		for (int i = 0; i < userList.size(); i++) {
			assertTrue(userList.get(i).equals(actualList.get(i)));
		}
	}

	@Test
	public void getUserByIdTest() throws UserException {
		User user = new User(1, "mouni", "mouni@gmail.com");
		Optional<User> expectedUser = Optional.of(user);
		when(userRepository.findById(user.getId())).thenReturn(expectedUser);
		User actualUser = userService.getUserById(1);
		assertTrue(actualUser.getName().equals(user.getName()));
	}

	@Test
	public void whenNoUserGetUserByIdTest() {
		given(this.userRepository.findById((long) 22)).willReturn(Optional.empty());
		assertThatCode(() -> userService.getUserById(22)).isInstanceOf(UserException.class)
				.hasMessageContaining("USER NOT FOUND WITH ID :22");
	}

	@Test
	public void createUserTest() {
		User user = new User(1, "mouni", "mouni@gmail.com");
		when(userRepository.save(Mockito.any())).thenReturn(user);
		assertTrue(userService.createUser(user).getName().equals(user.getName()));

	}

	@Test
	public void updateUserTest() throws UserException {
		User expectedUser = new User(1, "mouni", "mouni@gmail.com");
		when(userRepository.save(Mockito.any())).thenReturn(expectedUser);
		User actualUser = userService.updateUser(1, expectedUser);
		assertTrue(actualUser.getName().equals(expectedUser.getName()));

	}

	@Test
	public void WhenNoUserUpdateUserTest() {
		given(this.userRepository.findById((long) 22)).willReturn(Optional.empty());
		assertThatCode(() -> userService.updateUser(22, new User())).isInstanceOf(UserException.class)
				.hasMessageContaining("USER NOT FOUND WITH ID :22");
	}

	@Test
	public void deleteUserTest() throws UserException {
		User user = new User(1, "mouni", "mouni@gmail.com");
		Optional<User> expectedUser = Optional.of(user);
		when(userRepository.findById(user.getId())).thenReturn(expectedUser);
		userService.deleteUser(1);
	}

	@Test
	public void whenNoUserDeleteUserTest() throws UserException {
		given(this.userRepository.findById((long) 22)).willReturn(Optional.empty());
		assertThatCode(() -> userService.deleteUser(22)).isInstanceOf(UserException.class)
				.hasMessageContaining("USER NOT FOUND WITH ID :22");
	}
}
